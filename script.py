# -*- coding: iso-8859-1 -*-

'''
This script is a test to write little games on python for PSP using
https://code.google.com/p/pspstacklesspython/

(C) Copyright 2007 Francisco Jose Moreno Llorca <packo@assamita.net>

SmilEditor is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.



'''
import psp2d, pspos
import pspnet
import pspmp3
#import pspogg
from time import time, localtime
import datetime
import sys
import stackless

# Set processor and bus speed
pspos.setclocks(333,166)
#pspos.setclock(100)
#pspos.setbus(50)

print "Localtime: ", localtime()
print "Datetime: ", datetime.datetime.now()

#Dict: {"pregunta": ,"resp1": ,"resp2":  ,"resp3": ,"correcta":  }
preguntas = [{"pregunta": "�Cuantas horas dura la digestion","resp1":"1 hora" ,"resp2": "3 horas" ,"resp3": "15 minutos","correcta": 2 },
{"pregunta":"�Cuando hay que vacunarse de gripe?" ,"resp1": "Agosto","resp2":"Diciembre"  ,"resp3":"Octubre" ,"correcta": 3 },
{"pregunta":"�Como obtenemos vitaminas?" ,"resp1":"Con carne" ,"resp2":"Con fruta"  ,"resp3": "Con hamburguesas","correcta":  2}]

# Creates the screen and its background color (Black)
screen = psp2d.Screen()
screen.clear(psp2d.Color(255,255,255,255))

fondo =  psp2d.Image("fondo.png")

# Loads the font
font = psp2d.Font('font.png')

#load star
star = psp2d.Image("star.png")

#load gameover
gameover = psp2d.Image("gameover.png")

#load ok
ok = psp2d.Image("ok.png")

question = 0

# Creates the Agent base class
class Agent(object):
    def __init__(self):
        self.ch = stackless.channel()       # Communication channel (not used here)
        self.running = True                 # Flag to control the running status
        stackless.tasklet(self.runAction)() # Creates the agent tasklet

    def runAction(self):
        # Here we define the main action, a repetition of the function self.action()
        while self.running:
            # Runs the action
            self.action()
            # Give other tasklets its turn
            stackless.schedule()

    def action(self):
        # In the base class do nothing
        pass

class player(Agent):
    def __init__(self, rend):
        Agent.__init__(self)
        self.sprite = star
        self.rend = rend        # Reference to the renderer tasklet
        self.direction = 1
        self.speed = 30
        self.posX = 50
        self.posY = 130
        self.height = 32
        self.width = 32
        self.lastPad = time()
        self.boolSprite = False
        self.rend.agents.append(self) # Adds this agent to the renderer

        self.screenshot = 1;
        self.pos = 0
        self.c = False
        
        

    def action(self):
        #self.sprite = spriteImages[self.direction][int(self.boolSprite)]
        pad = psp2d.Controller()
        if pad.cross and self.c== False:
            global question
            print question
            print self.pos
            print preguntas[question]["correcta"]
            if self.pos == preguntas[question]["correcta"]:
                question = question+1
                self.pos = 0
                self.c = True
                self.posX = 50
                self.posY = 130
                self.height = 32
                self.width = 32
            else:
                print "Incorrecta"
                self.c = True
                question = -1
        elif pad.triangle:
            screen.saveToFile("ms0:/PSP/PICTURE/screenshot%s.png" % self.screenshot)
            self.screenshot += 1
            self.c = False
        elif pad.down and ((not self.lastPad or time() - self.lastPad >= 0.25) and self.pos <3 ) :
          #Draw the player facing south:
          self.pos = self.pos +1
          self.lastPad = time()
          self.direction = 1
          if self.posY + self.height + self.speed < 272:
            self.posY += self.speed
            self.boolSprite = not self.boolSprite
          self.c = False
        elif pad.up and ((not self.lastPad or time() - self.lastPad >= 0.25) and self.pos>1):
          #Draw the player facing north:
          self.c = False
          self.pos = self.pos - 1
          self.lastPad = time()
          self.direction = 0
          if self.posY - self.speed >= 0:
            self.posY -= self.speed
            self.boolSprite = not self.boolSprite


class render(Agent):
    # This is the renderer agent
    def __init__(self):
        Agent.__init__(self)
        self.agents = []

    def exit(self):
        # When the player calls the exit, tell all Agents to stop running
        print "Stopping agents..."
        for agent in self.agents:
            print "Stopped agent %s" % agent
            agent.running = False
        self.running = False
        print "Stopped self..."

    def action(self):
        # Each frame the renderer clears the screen,
        # writes the text and draws each registered agent.
        screen.clear(psp2d.Color(255,255,255,255))
        global question
        #print "En render: " + str(question)
        if question == len(preguntas):
            screen.blit(ok)
        elif question != -1:
            screen.blit(fondo)
        
            font.drawText(screen, 100, 130, preguntas[question]["pregunta"])
            font.drawText(screen, 150, 170, preguntas[question]["resp1"])
            font.drawText(screen, 150, 200, preguntas[question]["resp2"])
            font.drawText(screen, 150, 230, preguntas[question]["resp3"])
            
            for agent in self.agents:
                screen.blit(agent.sprite, 0, 0, agent.width,
                            agent.height, agent.posX, agent.posY, True)
        
        else:
            screen.blit(gameover)
        screen.swap()


print "Real memory: ",pspos.realmem()

#Loads background music
pspmp3.init(1)
pspmp3.load("music.mp3")        # Uncomment this to add a MP3 in backgound
pspmp3.play()

#Loads background music in ogg
#pspogg.init(2)
#pspogg.load('Oggsample.ogg')
#pspogg.play()

# Creates the renderer object
rend = render()
# Creates a player Agent
play = player(rend)
# Creates one NPC that runs around the screen
#NPC1 = NPC(rend)

# Starts the game loop
stackless.run()
#pspogg.end()
pspmp3.end()

sample-psp-game
===============

A sample game for psp written in python, it seems but its not a AAA game

It's a simple questions game written with https://code.google.com/p/pspstacklesspython/, it's just a test, and, why not I was boring that night and only that night ;)


Authors
-------
Francisco José Moreno Llorca <packo@assamita.net>

License
-------

(C) Copyright 2007 Francisco Jose Moreno LLorca


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.


Third Parties
-------------

music.mp3 - (C) Copyright DJRalph

